import { Component, OnInit, Output, Input, EventEmitter, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-displaypicture',
  templateUrl: 'base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})

export class BaseWidgetComponent implements OnInit {
  private image;
  private title;
  private media_displayName;
  private timestamp;
  private text;
  private newsId;

  private items: any;

  constructor() {

  }
  ngOnInit() {
    this.items = [
        {
            "image": "../assets/img/images2.jpg",
            "title":"Djokovic Menangi Laga Perdana",
            "media_displayName":"Indo Pos",
            "timestamp":"3 hours ago",
            "content":"– Juara dua kali Novak Djokovic langsung kerja keras di pertandingan pertamanya. Menghadapi Gilles Simon …",
            "newsId":"1"
        },
        {
            "image": "../assets/img/noimg.png",
            "title":"sdfg",
            "media_displayName":"sadf",
            "timestamp":"12",
            "content":"12",
            "newsId":"2"
        },
        {
            "image": "../assets/img/images3.jpg",
            "title":"Djokovic Menangi Laga Perdana 3",
            "media_displayName":"Indo Pos",
            "timestamp":"3 hours ago",
            "content":"– Juara dua kali Novak Djokovic langsung kerja keras di pertandingan pertamanya. Menghadapi Gilles Simon …",
            "newsId":"1"
        },
        {
            "image": "../assets/img/images.jpg",
            "title":"Djokovic Menangi Laga Perdana21",
            "media_displayName":"Indo Pos",
            "timestamp":"3 hours ago",
            "content":"– Juara dua kali Novak Djokovic langsung kerja keras di pertandingan pertamanya. Menghadapi Gilles Simon …",
            "newsId":"1"
        },
        {
            "image": "../assets/img/images1.jpg",
            "title":"Djokovic Menangi Laga Perdana",
            "media_displayName":"Indo Pos",
            "timestamp":"3 hours ago",
            "content":"– Juara dua kali Novak Djokovic langsung kerja keras di pertandingan pertamanya. Menghadapi Gilles Simon …",
            "newsId":"1"
        },
        
    ];

    this.onSelect(this.items[0]);
    
  }

  //buat load data baru
  ngOnChanges() {
    this.items = [];
  }

  onSelect(subitem) {
    console.log("this.items");
    if (subitem) {
      this.image = subitem.image;
      this.title = subitem.title;
      this.media_displayName = subitem.media_displayName;
      this.timestamp = subitem.timestamp;
      this.text = subitem.content;
      this.newsId = subitem.id;
    }
  }
}
